<?php
use Bitrix\Main\Config\Option;
if ( !$_REQUEST['PLACEMENT'] )
{
    $appData = [
        'TITLE' => 'EXTRA',
        'COMMENT' => 'Additional data management features during a call', // aka description
        //'GROUP_NAME' => 'group name'
        //'ADDITIONAL'
    ];
    return;
}
if ( empty($_POST) ) return;

define('NO_KEEP_STATISTIC', 'Y');
define('NO_AGENT_STATISTIC','Y');
define('NO_AGENT_CHECK', true);
define('STOP_STATISTICS', true);
define('PUBLIC_AJAX_MODE', true);
define('DisableEventsCheck', true);
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

//\Bitrix\Main\Loader::includeModule('main');
//\Bitrix\Main\Loader::includeModule('crm');
//\Bitrix\Main\Loader::includeModule('intranet');
CUtil::initJSCore(array('ajax', 'popup'));
// sessid: c6482f5db45304b8950c1bd4630645f2
// REST_APP_ID: 8
// PLACEMENT_OPTIONS[CALL_ID]: externalCall.f316f987a1013436aa898181af1e4ae5.1581525700
// PLACEMENT_OPTIONS[PHONE_NUMBER]: 1234567
// PLACEMENT_OPTIONS[LINE_NUMBER]
// PLACEMENT_OPTIONS[LINE_NAME]
// PLACEMENT_OPTIONS[CRM_ENTITY_TYPE]: LEAD
// PLACEMENT_OPTIONS[CRM_ENTITY_ID]: 28409
// PLACEMENT_OPTIONS[CRM_ACTIVITY_ID]: undefined
// PLACEMENT_OPTIONS[CALL_DIRECTION]: incoming
// PLACEMENT_OPTIONS[CALL_STATE]: connected
// PLACEMENT_OPTIONS[CALL_LIST_MODE]: false

global $USER, $APPLICATION;
if ( !$USER->IsAuthorized() ) die('Not correct auth');

if ( $_REQUEST['PLACEMENT_OPTIONS'] )
    $_REQUEST['PLACEMENT_OPTIONS'] = json_decode($_REQUEST['PLACEMENT_OPTIONS'], true);

if( !class_exists('\Dev\Call\Card') )
 \Bitrix\Main\Loader::includeModule('dev.card.show');
$module_id = \Dev\Call\Card::module;
$prefix = \Dev\Call\Card::prefix;

$debugMode = (Option::get($module_id, $prefix . "debug") == 'Y' && $USER->IsAdmin()) ? true : false;
$debugLabel = 'debugTime';

if($debugMode)
    \Bitrix\Main\Diag\Debug::startTimeLabel($debugLabel);
?>
    <meta name="robots" content="noindex, nofollow, noarchive">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <?
    //$APPLICATION->ShowCSS();
    $APPLICATION->ShowHeadStrings();
    $APPLICATION->ShowHeadScripts();
    $APPLICATION->showHead();
    //\Bitrix\Main\Page\Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/template_scripts.js", true);
    //$APPLICATION->SetUniqueJS('bx24', 'template');
    //$APPLICATION->SetUniqueCSS('bx24', 'template');

    $APPLICATION->IncludeComponent(
        'bitrix:crm.card.show.ext',
        '.default',
        [
            'ENTITY_TYPE' => $_REQUEST['PLACEMENT_OPTIONS']['CRM_ENTITY_TYPE'],
            'ENTITY_ID' =>  $_REQUEST['PLACEMENT_OPTIONS']['CRM_ENTITY_ID'],
            'PHONE_NUMBER' => $_REQUEST['PLACEMENT_OPTIONS']['PHONE_NUMBER'],
            //'RESULT' => $arResult,
            'PARAMS' => $_REQUEST['PLACEMENT_OPTIONS'],
            'ERRORS' => false,
            'DEBUG' => $debugMode
        ]
    );

    if($debugMode)
    {
        \Bitrix\Main\Diag\Debug::endTimeLabel($debugLabel);
        echo '<font color="white" style="background: red; position: absolute; bottom:23px; left:20px; opacity: 0.5; z-index: 105;">Execute Time: '. round(\Bitrix\Main\Diag\Debug::getTimeLabels()[$debugLabel]['time'], 3) .'</font>';
    }

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");