<?php

use Bitrix\Main\Application;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\ModuleManager;

Loc::loadMessages(__FILE__);

class dev_card_show extends CModule
{
    /**
     * @var string
     */
    private $templateName, $defaultTemplatesDir, $sourceTemplateDir, $destinationTemplateDir;
    private $componentName, $defaultComponentDir, $sourceComponentDir, $destinationComponentDir;

    public function __construct()
    {
        $this->MODULE_VERSION       = '1.0.7';
        $this->MODULE_VERSION_DATE  = '2020-02-18';
        $this->MODULE_ID            = 'dev.card.show';
        $this->MODULE_NAME          = 'Enhanced Call Card Show';
        $this->MODULE_DESCRIPTION   = 'Extension of standard component crm.card.show features';
        $this->MODULE_GROUP_RIGHTS  = 'N';
        $this->PARTNER_NAME         = 'Yuriev Yuri';
        $this->PARTNER_URI          = 'http://yurievyuri.ru';

        $this->componentName            = 'crm.card.show.ext';
        $this->defaultComponentDir      = '/components/bitrix/';
        $this->sourceComponentDir       = $_SERVER['DOCUMENT_ROOT'] . '/local/modules/' . $this->MODULE_ID . $this->defaultComponentDir  ;
        $this->destinationComponentDir  = $_SERVER['DOCUMENT_ROOT'] . '/local' . $this->defaultComponentDir;

        $this->templateName             = 'crm.card.show';
        $this->defaultTemplatesDir      = '/templates/bitrix24/components/bitrix/';
        $this->sourceTemplateDir        = $_SERVER['DOCUMENT_ROOT'] . '/local/modules/' . $this->MODULE_ID . $this->defaultTemplatesDir  ;
        $this->destinationTemplateDir   = $_SERVER['DOCUMENT_ROOT'] . '/bitrix' . $this->defaultTemplatesDir;
    }


    /**
     *
     */
    public function InstallFiles()
    {
        // create symlink template at the btrix24 template
        //if ( !readlink( $this->destinationTemplateDir . $this->templateName ) )
        //    if(!symlink( $this->sourceTemplateDir . $this->templateName, $this->destinationTemplateDir . $this->templateName ))
        //        die( __METHOD__ . ' : Failed to create link to component template. Correct operation of the application is not possible.');

        if ( !readlink( $this->destinationComponentDir . $this->componentName ) )
            if(!symlink( $this->sourceComponentDir . $this->componentName, $this->destinationComponentDir . $this->componentName ))
                die( __METHOD__ . ' : Failed to create link to component. Correct operation of the application is not possible.');
    }

    /**
     *
     */
    public function UnInstallFiles()
    {
        //        if( !unlink($this->destinationTemplateDir . $this->templateName) )
        //            die( __METHOD__ . ' : Failed to remove link for component template...');

        unlink( $this->destinationComponentDir . $this->componentName );

        //        if( !unlink($this->destinationComponentDir . $this->componentName) )
        //            die( __METHOD__ . ' : Failed to remove link for component...');
    }

    public function doInstall()
    {
        //$this->installDB();
        $this->InstallFiles();
        ModuleManager::registerModule($this->MODULE_ID);
        $this->installApp();
        echo \CAdminMessage::ShowNote( 'Module successfully installed' );
    }

    public function doUninstall()
    {
        //$this->uninstallDB();
        $this->UnInstallFiles();
        $this->unInstallApp();
        ModuleManager::unRegisterModule($this->MODULE_ID);
        echo \CAdminMessage::ShowNote( 'Module successfully uninstalled' );
    }

    public function installDB()
    {
        if (Loader::includeModule($this->MODULE_ID))
        {
        }
    }

    public function uninstallDB()
    {
        if (Loader::includeModule($this->MODULE_ID))
        {
            $connection = Application::getInstance()->getConnection();
            $connection->dropTable(DeveloperTable::getTableName());
        }
    }

    public function installApp()
    {
        if (!Loader::includeModule($this->MODULE_ID)) return false;
        if ( !class_exists('\Dev\Call\AppPlacements' )) return false;

        $object = new \Dev\Call\AppPlacements();
        $object->install();
        if ( $object->getERROR() ){
            echo \CAdminMessage::ShowNote( implode('<br>',$object->getERROR()) );
        }
    }
    public function unInstallApp()
    {
        if (!Loader::includeModule($this->MODULE_ID)) return false;
        if ( !class_exists('\Dev\Call\AppPlacements' )) return false;

        $object = new \Dev\Call\AppPlacements();
        $object->uninstall();
        if ( $object->getERROR() ){
            echo \CAdminMessage::ShowNote( implode('<br>',$object->getERROR()) );
        }
    }
}