<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Config\Option;
Loc::loadMessages(__FILE__);

/**
 * @property CCrmLead CrmEntity
 * @property string cacheTtl
 */
class CrmCardShowExtComponent extends CBitrixComponent
{
    const
        prefix = 'ahc_card_',
        module_id = 'dev.card.show'
    ;
    /**
     * @var int
     */
    private $countEvent;
    /**
     * @var array
     */
    private $allowedEventTypes;

    /**
     * @return array|mixed
     * @throws \Bitrix\Main\ArgumentNullException
     * @throws \Bitrix\Main\ArgumentOutOfRangeException
     * @throws \Bitrix\Main\LoaderException
     * @throws \Bitrix\Main\SystemException
     */
    public function executeComponent()
	{
	    global $USER;
		$this->arResult = array();
        $this->CrmEntity;

        $this->countEvent = Option::get( self::module_id, self::prefix . "count_event") ? : 3;
        $this->cacheTtl = self::getTtl();

        $this->allowedEventTypes = explode(',', Option::get( self::module_id, self::prefix . "allowed_event_types"));
        $this->allowedEventTypes[] = 'CRM_COMMENT';

		if (!$this->arParams['ENTITY_ID'])
		    $this->setErrors('No referring lead id', 'leadId');

        $this->getLeadData();

        // hack init
        if (!$this->arResult['ENTITY_DATA']['ENTITY_TYPE'])
            $this->arResult['ENTITY_DATA']['ENTITY_TYPE'] = 'LEAD';
        if( !$this->arResult['ENTITY_DATA']['ASSIGNED_BY'] || $this->arResult['ENTITY_DATA']['ASSIGNED_BY_ID'])
            $this->arResult['ENTITY_DATA']['ASSIGNED_BY'] = $this->arResult['ENTITY_DATA']['ASSIGNED_BY_ID'] = $USER->GetID();
        if ( !$this->arResult['ENTITY_DATA']['SOURCE_ID'] )
            $this->arResult['ENTITY_DATA']['SOURCE_ID'] = 'CALL';

        $this->crmEntityActivate();
        $this->getUserFieldsArray();
        $this->getSystemFields();
        $this->getMandatoryUserFields();
        $this->getHistoryHtml();
        $this->getUnSuccessStatuses();
        $this->getEntityDefaultPhones();
        $this->includeComponentTemplate();

        return $this->arResult;
	}

    /**
     * @return int
     * @throws \Bitrix\Main\ArgumentNullException
     * @throws \Bitrix\Main\ArgumentOutOfRangeException
     */
    static function getTtl()
    {
        return (int) Option::get(self::module_id, self::prefix . 'cache');
    }

    /**
     * @throws \Bitrix\Main\LoaderException
     */
    public function crmEntityActivate()
    {
        if( !class_exists('CCrmLead'))
            \Bitrix\Main\Loader::includeModule("crm");

        $class = $_REQUEST['ENTITY_TYPE'] ? '\CCrm'.ucfirst(strtolower($_REQUEST['ENTITY_TYPE'])) : '\CCrmLead';
        $this->CrmEntity = new $class(false);
        /*
            if( !$this->arResult['ENTITY_DATA']['ENTITY_TYPE'] || $this->arResult['ENTITY_DATA']['ENTITY_TYPE'] == 'LEAD')
                $this->CrmEntity = new \CCrmLead(false);
            elseif( $this->arResult['ENTITY_DATA']['ENTITY_TYPE'] == 'CONTACT')
                $this->CrmEntity = new \CCrmContact(false);
            elseif( $this->arResult['ENTITY_DATA']['ENTITY_TYPE'] == 'COMPANY')
                $this->CrmEntity = new \CCrmContact(false);
            elseif( $this->arResult['ENTITY_DATA']['ENTITY_TYPE'] == 'COMPANY')
                $this->CrmEntity = new \CCrmDeal(false);
        */
    }

    /**
     * @return array
     */
    public function getAllowedEventTypes()
    {
        return $this->allowedEventTypes;
    }

    /**
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\LoaderException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public function getEntityDefaultPhones()
    {
        if ( !class_exists('\Dev\Call\Card') )
            \Bitrix\Main\Loader::includeModule("dev.card.show");

        $this->arResult['DEFAULT_PHONES'] = [];

        $db = \Bitrix\Crm\FieldMultiTable::getList([
            'order'     => ['ID' => 'DESC'],
            'filter'    => ['ELEMENT_ID' => $this->arResult['ENTITY_DATA']['ID'] ? : $this->arResult['ID'], 'TYPE_ID' => 'PHONE'],
            'limit'     => 10
        ]);

        while ($item = $db->Fetch())
        {
            $item['DEFAULT'] = true;
            $item['VALUE_EXT'] = \Dev\Call\Card::phoneFormatEx($item['VALUE']);
            $this->arResult['DEFAULT_PHONES'][$item['ID']] = $item;
        }
    }

    /**
     * @param array $array
     * @return string
     */
    public function getHiddenInputs($array = [] )
    {
	    $return = '';
	    foreach($array as $key => $value)
	    {
            if ( $key[0] != '~' )
            {
                if( is_array($value) )
                    $value = base64_encode(json_encode($value));

                $return .= "<input type=hidden name={$key} value={$value} />";
            }
        }

	    return $return;
    }

    /**
     * @param $value
     * @param bool $key
     * @return bool
     */
    private function setErrors($value, $key = false)
    {
        if(!$value) return false;

        $this->arResult['ERROR'][$key] = $value;
    }

    /**
     * @return mixed
     */
    public function getErrors()
    {
	    return $this->arResult['ERROR'];
    }

    /**
     * @return array
     * @throws \Bitrix\Main\LoaderException
     */
    public function getLeadData()
    {
        if( !class_exists('CCrmLead'))
            \Bitrix\Main\Loader::includeModule("crm");

        $filter = [

            'ID' => $this->arParams['ENTITY_ID'],
            'CHECK_PERMISSIONS' => 'N'
        ];

        return $this->arResult['ENTITY_DATA'] = \CCrmLead::GetList( false, $filter )->Fetch();
    }

    /**
     * @param bool $propName
     * @param array $arResult
     * @return array
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\LoaderException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    static function getEnumList( $propName = false, $arResult = [] )
    {
        if( !$propName ) return [];
        $cacheId = __METHOD__ . $propName .'_zx';
        $cacheTtl = self::getTtl();
        $cache = \Bitrix\Main\Application::getInstance()->getManagedCache();
        $nameFormat = \CSite::getNameFormat(null, 'S1');
        $groupsResponsible = explode(',', Option::get(self::module_id, self::prefix . 'groups_resposible'));

        //$map[] = '...';
        $map = [];

        if ($cache->read($cacheTtl, $cacheId) && $propName != 'ASSIGNED_BY_ID' )
            return $cache->get($cacheId);

        if ( $propName == 'SOURCE_ID' )
        {
            if (!class_exists('CCrmStatus'))
                \Bitrix\Main\Loader::includeModule("crm");

            foreach( \CCrmStatus::GetStatusListEx('SOURCE') as $key => $value )
                $map[$key] = $value;
        }
        elseif ( $propName == 'ASSIGNED_BY_ID' /*&& $arResult['ENTITY_DATA']['ASSIGNED_BY_ID']*/ )
        {
            /*$dbResult = \Bitrix\Main\UserTable::getList([
                'order'     => ['NAME' => 'ASC'],
                'select'    => ['LAST_NAME', 'NAME', 'ID', 'LOGIN', 'EMAIL', 'PERSONAL_PHOTO'],
                'filter'    => [
                ],
                'limit'     =>  300
            ]);*/

            $filter = [
                'ACTIVE'            => 'Y' ,
                'GROUP_ID'          => $groupsResponsible,
                '!UF_DEPARTMENT'    => false,
                '!NAME'             => false,
                //'ID' => $arResult['ENTITY_DATA']['ASSIGNED_BY_ID']
            ];

            $dbResult = \CUser::GetList(($by="name"), ($order="asc"), $filter);

            while($list = $dbResult->Fetch())
                $map[ $list['ID'] ] = trim(\CUser::formatName($nameFormat, $list, true, false));
        }
        else
        {
            $userFieldEnumEntity = new \CUserFieldEnum();
            $dbResult = $userFieldEnumEntity->GetList(array('VALUE' => 'ASC'), array('USER_FIELD_NAME' => $propName));
            while($list = $dbResult->Fetch())
                $map[ $list['ID'] ] = $list['VALUE'];
        }

        $cache->set($cacheId, $map);

        return $map;
    }

    /**
     * @param bool $propName
     * @param bool $selected
     * @param string $option
     * @param array $arResult
     * @return string
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\LoaderException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    static function getSelectOptions( $propName = false, $selected = false, $option = 'option', $arResult = [] )
    {
        if( !$propName ) return 'error';

        $return = '';

        foreach ( self::getEnumList($propName, $arResult) as $id => $name )
        {
            $selectValue = '';

            //if( $selected == $id )
            //    $selectValue = 'selected="selected"';

            if ( $option == 'ui' )
            {
                $selectValue .= 'class="item ';
                if ($selected == $id)
                    $selectValue .= 'selected';
                $selectValue.= '"';
            }

            if  ($option = 'ui')
                $return .= '<div ' . $selectValue . ' data-value="' . $id . '">' . $name . '</div>';
            else
                $return .= '<option ' . $selectValue . ' value="' . $id . '">' . $name . '</option>';
        }

        return $return;
    }

    /**
     * @param bool $propName
     * @param array $arResult
     * @param int $tabIndex
     * @return string
     * @throws \Bitrix\Main\LoaderException
     * @throws \Bitrix\Main\SystemException
     */
    public function createSelect( $propName = false, $arResult = [], $tabIndex = 0 )
    {
        if( !$propName ) return 'no exist name';

        $required = '';

        if( in_array($propName,$arResult['USER_FIELDS']['MANDATORY']) )
            $required = 'required="required"';

        $result = '';
        $result.= '<select '.$required.' name="' . $propName . '" tabindex="' . $tabIndex . '">';
        $result.= self::getSelectOptions( $propName, $arResult['ENTITY_DATA'][ $propName ] );
        $result.= '</select>';

        return $result;
    }

    /**
     * @param bool $propName
     * @param array $arResult
     * @param int $tabIndex
     * @param bool $required
     * @param string $addedStyles
     * @return string
     * @throws \Bitrix\Main\LoaderException
     * @throws \Bitrix\Main\SystemException
     */
    public function createSelectUI($propName = false, $arResult = [], $tabIndex = 0, $required = false, $addedStyles = '' )
    {
        if( !$propName ) return 'no exist name';

        if( in_array($propName,$arResult['USER_FIELDS']['MANDATORY']) || $required == true )
            $required = 'required="required"';

        $result = '';
        $result.= '<div class="ui fluid search selection dropdown ' .$addedStyles. '">';

        $propValue = $arResult['ENTITY_DATA'][ $propName ];

        $result.= '<input type="hidden" '.$required.' name="'.$propName.'"  '.$required.'  value="'. $propValue .'">';

        $result.= '<div class="default text"><small>select a '. strtolower(self::prefetchCorrectFieldName($propName, $arResult)) . '</small></div>';

        $result.= '<i class="dropdown icon"></i>';

        $result.= '<div class="menu">';

            $result .= self::getSelectOptions( $propName, $arResult['ENTITY_DATA'][ $propName ], 'ui', $arResult );

        $result.= '</div>';

        $result.= '</div>';

        return $result;
    }

    /**
     * @param $propName
     * @param array $arResult
     * @param int $tabIndex
     * @param bool $required
     * @return string
     */
    public function createInputUI($propName, $arResult = [], $tabIndex = 0, $required = false )
    {
        if( !$propName ) return 'no exist name';

        if( in_array($propName,$arResult['USER_FIELDS']['MANDATORY']) || $required == true )
            $required = 'required="required"';

        $result = '';
        $name = self::prefetchCorrectFieldName($propName, $arResult);

        $result .= '<label><small>'.self::prefetchCorrectFieldName($propName, $arResult).'</small></label>';
            $result .= '<div class="ui icon input">';
            $result .= '<input name="'.$propName.'" '
                . $required . ' class="" type="text" tabindex="'
                . $tabIndex . '" placeholder="Add a '
                . $name . '" value="'
                . $arResult['ENTITY_DATA'][$propName] .'" />';
        $result .= '</div>';

        return $result;
    }

    /**
     * data
     * @param bool $propName
     * @param array $arResult
     * @return bool|string
     */
    public function prefetchCorrectFieldName($propName = false, $arResult = [] )
    {
        if ( !$arResult['ENTITY_DATA']['ENTITY_TYPE'] ) return false;

        $return = key_exists($propName, $arResult['USER_FIELDS'][ $arResult['ENTITY_DATA']['ENTITY_TYPE'] ])
            ?   $arResult['USER_FIELDS'][ $arResult['ENTITY_DATA']['ENTITY_TYPE'] ][ $propName ]['EDIT_FORM_LABEL']
            :   $arResult['SYSTEM_FIELDS'][ $arResult['ENTITY_DATA']['ENTITY_TYPE'] ][ $propName ]
        ;

        return $return ? : ucwords($propName);
    }

    /**
     * @param bool $propName
     * @return mixed
     */
    public function getEntityData( $propName = false )
    {
        return $this->arResult['ENTITY_DATA'][ $propName ];
    }

    /**
     * @return array|bool
     * @throws \Bitrix\Main\ArgumentNullException
     * @throws \Bitrix\Main\ArgumentOutOfRangeException
     * @throws \Bitrix\Main\SystemException
     */
    public function getSystemFields()
    {
        if ( !$this->CrmEntity ) return false;

        $cacheId = __METHOD__;
        $cacheTtl = self::getTtl();
        $cache = \Bitrix\Main\Application::getInstance()->getManagedCache();

        if ($cache->read($cacheTtl, $cacheId))
            return $this->arResult['SYSTEM_FIELDS'][ $this->arResult['ENTITY_DATA']['ENTITY_TYPE'] ] = $cache->get($cacheId);

        $return = [];
        foreach ( $this->CrmEntity->GetFieldsInfo() as $key => $value )
            $return[$key] = $this->CrmEntity->GetFieldCaption($key);

        $cache->set($cacheId, $return);

        return $this->arResult['SYSTEM_FIELDS'][ $this->arResult['ENTITY_DATA']['ENTITY_TYPE'] ] = $return;
    }

    /**
     * @param string $entity
     * @return array|mixed
     * @throws \Bitrix\Main\SystemException
     */
    public function getUserFieldsArray( $entity = 'LEAD' )
    {
        $cacheId = __METHOD__ . $entity . '_l';
        $cacheTtl = self::getTtl();
        $cache = \Bitrix\Main\Application::getInstance()->getManagedCache();

        if ($cache->read($cacheTtl, $cacheId))
            return $this->arResult['USER_FIELDS'][$this->arResult['ENTITY_DATA']['ENTITY_TYPE']] = $cache->get($cacheId);

        global $USER_FIELD_MANAGER;
        $data = $USER_FIELD_MANAGER->GetUserFields('CRM_'.$entity, false, 'en');

        $cache->set($cacheId, $data);
        return $this->arResult['USER_FIELDS'][$this->arResult['ENTITY_DATA']['ENTITY_TYPE']] = $data;
    }

    /**
     * @param string $entity
     * @return array
     * @throws \Bitrix\Main\ArgumentNullException
     * @throws \Bitrix\Main\ArgumentOutOfRangeException
     * @throws \Bitrix\Main\SystemException
     */
    public function getMandatoryUserFields($entity = 'LEAD' )
    {
        $parentKey = 'USER_FIELDS';
        $childKey = 'MANDATORY';
        $cacheId = __METHOD__ . $entity . '_s';
        $cacheTtl = self::getTtl();
        $cache = \Bitrix\Main\Application::getInstance()->getManagedCache();

        if ($cache->read($cacheTtl, $cacheId))
            return $this->arResult[ $parentKey ][ $childKey ] = $cache->get($cacheId);

        $data = [];
        $arFilter = ['ENTITY_ID' => 'CRM_' . $entity, 'MANDATORY' => 'Y'];
        $rs = \CUserTypeEntity::GetList(array(), $arFilter);
        while($arUserField = $rs->Fetch())
            $data[] = $arUserField['FIELD_NAME'];

        $cache->set($cacheId, $data);

        return $this->arResult[ $parentKey ][ $childKey ] = $data;
    }

    /**
     * @param bool $ufName
     * @param string $entity
     * @param array $arResult
     * @return string
     */
    public function getUserFieldName( $ufName = false, $entity = 'LEAD', $arResult = [] )
    {
        if ( !$ufName ) return 'no key';

        if ( !$arResult)
            return $this->arResult['USER_FIELDS'][ $entity ][ $ufName ]['EDIT_FORM_LABEL'] ? : 'error';
        else
            return $arResult['USER_FIELDS'][ $entity ][ $ufName ]['EDIT_FORM_LABEL'] ? : 'error';
    }

    /**
     * @return \Bitrix\Crm\Activity\Provider\Base[]
     * @throws \Bitrix\Main\ArgumentNullException
     * @throws \Bitrix\Main\ArgumentOutOfRangeException
     * @throws \Bitrix\Main\SystemException
     */
    public function getProviders()
    {
        $cacheId = __METHOD__ . '_cache';
        $cacheTtl = self::getTtl();
        $cache = \Bitrix\Main\Application::getInstance()->getManagedCache();

        if ($cache->read($cacheTtl, $cacheId))
            return $this->arResult['PROVIDERS'] = $cache->get($cacheId);

        $provArray = \Bitrix\Crm\Activity\Provider\ProviderManager::getProviders();
        foreach ( $provArray as $key => $value )
            $provArray[$key] = $value::getName();

        // fixed comment
        $provArray['CRM_COMMENT'] = 'Comment';
        $provArray['ANY'] = 'Activity';

        $cache->set($cacheId, $provArray);

        return $this->arResult['PROVIDERS'] = $provArray;
    }


    /**
     * @return array|bool|false
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public function getHistory()
    {
        if( !class_exists('CCrmTimelineComponent'))
            \CBitrixComponent::includeComponentClass("bitrix:crm.timeline");

        if ( !$this->arResult['ENTITY_DATA'] ) return false;

        $data = new \CCrmTimelineComponentEx();

        $entityId = $this->arResult['ENTITY_DATA']['ID'];

        if ( $this->arResult['ENTITY_DATA']['ENTITY_TYPE'] == 'CONTACT' )
            $typeId = 3;
        else
            $typeId = 1;

        $data->setEntityTypeID( $typeId ); // 3 Company
        $data->setEntityID($entityId);

        //$data->prepareScheduleItems();
        //$data->prepareHistoryFilter();
        //$data->prepareHistoryItems();
        //$data->prepareChatData();

        $nextOffsetTime = null;
        $nextOffsetID = 0;

        // todo фильтр типов
        // $this->allowedEventTypes

        return $this->arResult['HISTORY'] = $data->loadHistoryItemsEx(
            0,
            $nextOffsetTime,
            0,
            $nextOffsetID,
            [
                'limit' => $this->countEvent,
                'filter' => []
            ],
            [
                'filter' => [
                    //'=ASSOCIATED_ENTITY_CLASS_NAME' => 'VOXIMPLANT_CALL'
                ]
            ]
        );
    }

    /**
     * @return bool|string|string[]
     * @throws \Bitrix\Main\ArgumentNullException
     * @throws \Bitrix\Main\ArgumentOutOfRangeException
     * @throws \Bitrix\Main\ObjectException
     * @throws \Bitrix\Main\SystemException
     */
    public function getHistoryHtml()
    {
        $this->getHistory();
        if ( !$this->arResult['HISTORY'] ) return false;

        $this->getProviders();

        if ( !$this->arResult['PROVIDERS']) return false;

        $result = '<div class="ui list">';

        foreach( $this->arResult['HISTORY'] as $value )
        {
            if( !$value['ASSOCIATED_ENTITY_CLASS_NAME'] && ($value['COMMENT'] || $value['TITLE']))
                $value['ASSOCIATED_ENTITY_CLASS_NAME'] = 'ANY';

            if ( !$value['ASSOCIATED_ENTITY_CLASS_NAME'] ) continue;

            if ( !in_array($value['ASSOCIATED_ENTITY_CLASS_NAME'], $this->allowedEventTypes) ) continue;

            $result.=   '<div class="item"><small>';

                $result.=   '<div class="content">';

                    $result.=   '<span class="header">';

                        $result .= self::getIcon($value['ASSOCIATED_ENTITY_CLASS_NAME']) ;

                        $result .= (new \Bitrix\Main\Type\DateTime($value['CREATED']))->toUserTime() ;

                    $result .= '</span>';

                    if ( trim($this->arResult['PROVIDERS'][ $value['ASSOCIATED_ENTITY_CLASS_NAME'] ]) )
                        $result .=   '<font color="#008b8b">' . $this->arResult['PROVIDERS'][ $value['ASSOCIATED_ENTITY_CLASS_NAME'] ] . '</font>';

                    if($value['TITLE'])
                        $result .=   ' ' . $value['TITLE'];

                    if($value['LEGEND'])
                        $result .=   ' ' . $value['LEGEND'];

                    $result.=   ', <a target="_blank" href="'.$value['AUTHOR']['SHOW_URL'] .'">'. $value['AUTHOR']['FORMATTED_NAME']. '</a>';

                    $result.=   '<div class="description">';

                    if($value['COMMENT'])
                        $result.= '<span style="background: lightgoldenrodyellow;">"' . $value['COMMENT'] . '"</span>';

                    if ( $value['ASSOCIATED_ENTITY'])
                    {
                        if ($value['ASSOCIATED_ENTITY']['CALL_INFO'] )
                        {
                            $result.=$value['ASSOCIATED_ENTITY']['CALL_INFO']['CALL_TYPE_TEXT'];
                        }
                        else
                        {
                            $result.=$value['ASSOCIATED_ENTITY']['SUBJECT'];
                        }

                        if( $value['ASSOCIATED_ENTITY']['DESCRIPTION_RAW'] )
                            $result.=  '<span style="background: lightgoldenrodyellow;">"' . $value['ASSOCIATED_ENTITY']['DESCRIPTION_RAW'] . '"</span>';
                    }

                    $result.= '</div>';

            $result.= '</div>';

            $result.=  '</small></div>';
        }

        $result.= '</div>';

        return $this->arResult['HISTORY_HTML'] = htmlspecialcharsEx($result);
    }

    /**
     * @param bool $type
     * @return string
     */
    public function getIcon($type = false )
    {
        switch ( $type )
        {
            case 'VOXIMPLANT_CALL' :
                $class = 'ui-icon-service-call-up';
                break;
            case 'CALL_LIST' :
                $class = 'ui-icon-service-callback';
                break;
            case 'CRM_EMAIL' :
                $class = 'ui-icon-service-email';
                break;
            case 'CRM_SMS' :
                $class = 'ui-icon-service-imessage';
                break;

            case 'IMOPENLINES_SESSION' :
                $class = 'ui-icon-service-livechat';
                break;

            case 'CRM_WEBFORM' :
                $class = 'ui-icon-service-webform';
                break;

            case 'CRM_LF_MESSAGE':
                $class = 'ui-icon-service-universal';
                break;

            case 'CRM_EXTERNAL_CHANNEL':
                $class = 'ui-icon-service-directline';
                break;

            case 'CRM_REQUEST':
                $class = 'ui-icon-service-site-b24';
                break;

            default :
                $class = 'ui-icon-service-ya-direct';
                break;
        }

        return  '<span class="crm-tracking-entity-path-icon ui-icon '.$class.'"><i></i></span>'; // '<i class="map marker icon"></i>'; //
    }

    /**
     * @return array
     * @throws \Bitrix\Main\ArgumentNullException
     * @throws \Bitrix\Main\ArgumentOutOfRangeException
     * @throws \Bitrix\Main\SystemException
     * @throws \Bitrix\Main\LoaderException
     */
    public function getUnSuccessStatuses()
    {
        $key = 'UNSUCCESS_FIELDS';

        $cacheId = __METHOD__ . $key . 'x';
        $cacheTtl = (self::getTtl() / 14);
        $cache = \Bitrix\Main\Application::getInstance()->getManagedCache();

        if ($cache->read($cacheTtl, $cacheId))
            return $this->arResult['JUNK'] = $cache->get($cacheId);

        $result = [];

        foreach ( self::getStatusList($key) as $v )
            $result[ $v['STATUS_ID'] ] = $v['NAME'];

        $cache->set( $cacheId, $result );

        return $this->arResult['JUNK'] = $result;
    }

    /**
     * @param string $type
     * @return mixed
     * @throws \Bitrix\Main\LoaderException
     */
    static function getStatusList($type = 'UNSUCCESS_FIELDS' )
    {
        $array['HEADERS'] = [];
        $array['ROWS'] = [];
        $array['ENTITY'] = [];
        $settings = [];
        $colorSchemes = [];

        if ( !class_exists('CCrmStatus') )
            \Bitrix\Main\Loader::includeModule('crm');

        foreach(\CCrmStatus::GetEntityTypes() as $entityId => $arEntityType)
        {
            $array['HEADERS'][$entityId] = $arEntityType['NAME'];
            $array['ROWS'][$entityId] = Array();

            if(isset($arEntityType['SEMANTIC_INFO']) && is_array($arEntityType['SEMANTIC_INFO']))
                $array['ENTITY'][$entityId] = $arEntityType['SEMANTIC_INFO'];

            $colorScheme = \Bitrix\Crm\Color\PhaseColorSchemeManager::resolveSchemeByName('CONFIG_STATUS_'.$entityId);

            if($colorScheme)
                $colorSchemes[$entityId] = $colorScheme;
            else
                $settings[$entityId] = unserialize(\COption::GetOptionString('crm', 'CONFIG_STATUS_'.$entityId));
        }

        $res = \CCrmStatus::GetList(array('SORT' => 'ASC'), ['=ENTITY_ID' => 'STATUS']);
        while($status = $res->Fetch())
        {
            $array['ROWS'][$status['ENTITY_ID']][$status['ID']] = $status;
            $entityId = $status['ENTITY_ID'];
            if(isset($colorSchemes[$entityId]))
            {
                $colorSchemeElement = $colorSchemes[$entityId]->getElementByName($status['STATUS_ID']);
                if($colorSchemeElement)
                    $array['ROWS'][$entityId][$status['ID']]['COLOR'] = $colorSchemeElement->getColor();
            }
            elseif(!empty($settings))
                $array['ROWS'][$status['ENTITY_ID']][$status['ID']]['COLOR'] = $settings[$status['ENTITY_ID']][$status['STATUS_ID']]['COLOR'];

            if($array['ENTITY'][$status['ENTITY_ID']]['FINAL_SUCCESS_FIELD'] == $status['STATUS_ID'])
                $array['ENTITY'][$status['ENTITY_ID']]['FINAL_SORT'] = $status['SORT'];
        }

        foreach( $array['ENTITY'] as $entityId => $dataEntity )
        {
            $array['INITIAL_FIELDS'][$entityId] = array();
            $array['EXTRA_FIELDS'][$entityId] = array();
            $array['FINAL_FIELDS'][$entityId] = array();
            $array['EXTRA_FINAL_FIELDS'][$entityId] = array();
            $array['SUCCESS_FIELDS'][$entityId] = array();
            $array['UNSUCCESS_FIELDS'][$entityId] = array();
            $number = 1;
            foreach($array['ROWS'][$entityId] as $status)
            {
                $status['NUMBER'] = $number;
                if (empty($array['INITIAL_FIELDS'][$entityId]))
                {
                    $array['INITIAL_FIELDS'][$entityId] = $status;
                    $array['SUCCESS_FIELDS'][$entityId][] = $status;
                }
                elseif($status['STATUS_ID'] == $dataEntity['FINAL_SUCCESS_FIELD'])
                {
                    $array['FINAL_FIELDS'][$entityId]['SUCCESSFUL'] = $status;
                    $array['SUCCESS_FIELDS'][$entityId][] = $status;
                }
                elseif($status['STATUS_ID'] == $dataEntity['FINAL_UNSUCCESS_FIELD'])
                {
                    $array['FINAL_FIELDS'][$entityId]['UNSUCCESSFUL'] = $status;
                    $array['UNSUCCESS_FIELDS'][$entityId][] = $status;
                }
                else
                {
                    if($status['SORT'] < $array['ENTITY'][$status['ENTITY_ID']]['FINAL_SORT'])
                    {
                        $array['EXTRA_FIELDS'][$entityId][] = $status;
                        $array['SUCCESS_FIELDS'][$entityId][] = $status;
                    }
                    else
                    {
                        $array['EXTRA_FINAL_FIELDS'][$entityId][] = $status;
                        $array['UNSUCCESS_FIELDS'][$entityId][] = $status;
                    }
                }
                $number++;
            }
        }

        if ( isset( $array[ $type ] ) )
            return $array[ $type ]['STATUS'];
        else
            return $array;
    }
}

CBitrixComponent::includeComponentClass("bitrix:crm.timeline");

/**
 * Class CCrmTimelineComponentEx
 */
class CCrmTimelineComponentEx extends CCrmTimelineComponent
{
    /**
     * @param $offsetTime
     * @param $nextOffsetTime
     * @param $offsetID
     * @param $nextOffsetID
     * @param array $params
     * @param array $extra
     * @return array|false
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public function loadHistoryItemsEx($offsetTime, &$nextOffsetTime, $offsetID, &$nextOffsetID, array $params = [], array $extra = [])
    {
        if($this->entityID <= 0) return array();

        $limit = isset($params['limit']) ? (int)$params['limit'] : 0;
        $onlyFixed = isset($params['onlyFixed']) && $params['onlyFixed'] == true;
        $filter = isset($params['filter']) && is_array($params['filter']) ? $params['filter'] : array();

        //Permissions are already checked
        $query = new \Bitrix\Main\Entity\Query( \Bitrix\Crm\Timeline\Entity\TimelineTable::getEntity() );
        $query->addSelect('*');

        $bindingQuery = new \Bitrix\Main\Entity\Query( \Bitrix\Crm\Timeline\Entity\TimelineBindingTable::getEntity() );
        $bindingQuery->addSelect('OWNER_ID');
        $bindingQuery->addFilter('=ENTITY_TYPE_ID', $this->entityTypeID);
        $bindingQuery->addFilter('=ENTITY_ID', $this->entityID);

        if($onlyFixed)
            $bindingQuery->addFilter('=IS_FIXED', 'Y');

        $bindingQuery->addSelect('IS_FIXED');
        $query->addSelect('bind.IS_FIXED', 'IS_FIXED');

        $query->registerRuntimeField('',
            new \Bitrix\Main\Entity\ReferenceField('bind',
                \Bitrix\Main\Entity\Base::getInstanceByQuery($bindingQuery),
                array('=this.ID' => 'ref.OWNER_ID'),
                array('join_type' => 'INNER')
            )
        );

        //Client filter
        /*
        $bindingQuery1 = new Query(TimelineBindingTable::getEntity());
        $bindingQuery1->addSelect('OWNER_ID');

        $bindingQuery1->where(
            Main\Entity\Query::filter()
                ->where('ENTITY_TYPE_ID', '=', 4)
                ->where('ENTITY_ID', '=', 2414)
        );

        $query->registerRuntimeField('',
            new ReferenceField('bind1',
                Base::getInstanceByQuery($bindingQuery1),
                array('=this.ID' => 'ref.OWNER_ID'),
                array('join_type' => 'INNER')
            )
        );
        */

        if(isset($filter['CREATED_to']))
            $filter['CREATED_to'] = \Bitrix\Main\Type\DateTime::tryParse($filter['CREATED_to']);

        if(isset($filter['CREATED_from']))
            $filter['CREATED_from'] = \Bitrix\Main\Type\DateTime::tryParse($filter['CREATED_from']);

        if($offsetTime instanceof \Bitrix\Main\Type\DateTime && (!isset($filter['CREATED_to']) || $offsetTime < $filter['CREATED_to']))
            $filter['CREATED_to'] = $offsetTime;

        if(!empty($filter))
        {
            $entityFilter = $this->getHistoryFilter();
            $entityFilter->prepareListFilterParams($filter);
            \Bitrix\Crm\Filter\TimelineDataProvider::prepareQuery($query, $filter);
        }

        $query->whereNotIn(
            'ASSOCIATED_ENTITY_TYPE_ID',
            \Bitrix\Crm\Timeline\TimelineManager::getIgnoredEntityTypeIDs()
        );

        $query->setOrder(
            [
                'CREATED'   => 'DESC',
                'ID'        => 'DESC'
            ]
        );

        // added дополнительные фильтры
        if ( !empty($extra['filter']) )
            foreach ( $extra['filter'] as $key => $value)
                $query->addFilter($key,$value);

        if($limit > 0)
            $query->setLimit($limit);

        $items = array();
        $itemIDs = array();
        $offsetIndex = -1;
        $dbResult = $query->exec();
        while($fields = $dbResult->fetch())
        {
            $itemID = (int)$fields['ID'];
            $items[] = $fields;
            $itemIDs[] = $itemID;

            if($offsetID > 0 && $itemID === $offsetID)
            {
                $offsetIndex = count($itemIDs) - 1;
            }
        }
        if($offsetIndex >= 0)
        {
            $itemIDs = array_slice($itemIDs, $offsetIndex + 1);
            $items = array_splice($items, $offsetIndex + 1);
        }

        $nextOffsetTime = null;
        if(!empty($items))
        {
            $item = $items[count($items) - 1];
            if(isset($item['CREATED']) && $item['CREATED'] instanceof DateTime)
            {
                $nextOffsetTime = $item['CREATED'];
                $nextOffsetID = (int)$item['ID'];
            }
        }

        $itemsMap = array_combine($itemIDs, $items);
        \Bitrix\Crm\Timeline\TimelineManager::prepareDisplayData($itemsMap, $this->userID, $this->userPermissions);
        return array_values($itemsMap);
    }

}