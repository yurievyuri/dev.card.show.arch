<div type="folder" class="<?=pathinfo(__FILE__)['filename']?>" style="display:none">

    <h5 class="ui header">
        <i class="<?=explode('|',$additionalButtons['contact'])[1]?>"></i>
        <div class="content">
            Convert Current Lead to Contact
        </div>
    </h5>

    <div class="field">
        <div class="ui toggle checkbox">
            <input type="checkbox" name="MOVE_LEAD" value="">
            <label>Confirm Action</label>
        </div>
    </div>
    <div class="field">
        <div class="ui toggle checkbox">
            <input type="checkbox" name="DELETE_LEAD" value="">
            <label>Delete This Lead</label>
        </div>
    </div>

</div>