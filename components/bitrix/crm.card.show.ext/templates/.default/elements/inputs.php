
<!--div class="crm-entity-card-widget-title">
    <div style="max-width: calc(100% - 30px); min-width: 0px; flex: 1 1 0%;">
        <span class="crm-entity-card-widget-title-text">Lead extra information</span>
    </div>
    <div class="crm-entity-widget-actions-block"><span class="crm-entity-widget-hide-btn">Cancel</span></div>
</div-->

<div class="form-row crm-entity-card-widget-title">
    <div style="max-width: calc(100% - 30px); min-width: 0px; flex: 1 1 0%;">
        <span class="crm-entity-card-widget-title-text">Entity Data:</span>
    </div>
    <!--div class="crm-entity-widget-actions-block"><span class="crm-entity-widget-hide-btn">Cancel</span></div-->
</div>

<table class="inputExtraForm">
    <tr>
        <td>
            <div class="crm-entity-widget-content-block crm-entity-widget-content-block-field-text" cid="NAME">
                <div class="crm-entity-widget-content-block-title">
                    <span class="crm-entity-widget-content-block-title-text">Name</span>
                </div>
                <div class="crm-entity-widget-content-block-inner">
                    <div class="crm-entity-widget-content-block-field-container">
                        <input name="NAME" class="crm-entity-widget-content-input" type="text" tabindex="0" value="<?=$arResult['ENTITY_DATA']['NAME']?>" />
                    </div>
                </div>
            </div>
        </td>
        <td>
            <div class="crm-entity-widget-content-block crm-entity-widget-content-block-field-text" cid="LAST_NAME">
                <div class="crm-entity-widget-content-block-title">
                    <span class="crm-entity-widget-content-block-title-text">Last Name</span>
                </div>
                <div class="crm-entity-widget-content-block-inner">
                    <div class="crm-entity-widget-content-block-field-container">
                        <input name="LAST_NAME" class="crm-entity-widget-content-input" type="text" tabindex="1" value="<?=$arResult['ENTITY_DATA']['LAST_NAME']?>" />
                    </div>
                </div>
            </div>
        </td>
    </tr>
    <tr>
        <td>
            <?$propName = \Bitrix\Main\Config\Option::get($module_id, $prefix . "prop_lead_type");?>
            <div class="crm-entity-widget-content-block crm-entity-widget-content-block-field-custom-select"
                 data-cid="<?=$propName?>">
                <div data-field-tag="<?=$propName?>" class="crm-entity-widget-before-action"></div>
                <div class="crm-entity-widget-content-block-title">
                    <span class="crm-entity-widget-content-block-title-text"><?=$component->getUserFieldName($propName);?></span>
                </div>
                <div class="crm-entity-widget-content-block-inner">
                        <span class="fields enumeration field-wrap">
                            <span class="fields enumeration enumeration-select field-item">
                                <?=CrmCardShowExtComponent::createSelect(
                                    $propName, $arResult, 2
                                );?>
                            </span>
                        </span>
                </div>
                <div class="crm-entity-widget-content-block-context-menu"></div>
            </div>
        </td>
        <td>
            <?$propName = \Bitrix\Main\Config\Option::get($module_id, $prefix . "prop_market_type");?>
            <div class="crm-entity-widget-content-block crm-entity-widget-content-block-field-custom-select"
                 data-cid="<?=$propName?>">
                <div data-field-tag="<?=$propName?>" class="crm-entity-widget-before-action"></div>
                <div class="crm-entity-widget-content-block-title">
                    <span class="crm-entity-widget-content-block-title-text"><?=$component->getUserFieldName($propName);?></span>
                </div>
                <div class="crm-entity-widget-content-block-inner">
                        <span class="fields enumeration field-wrap">
                            <span class="fields enumeration enumeration-select field-item">
                                <?=CrmCardShowExtComponent::createSelect(
                                    $propName, $arResult, 2
                                );?>
                            </span>
                        </span>
                </div>
                <div class="crm-entity-widget-content-block-context-menu"></div>
            </div>
        </td>
    </tr>

    <tr>
        <td>
            <div class="crm-entity-widget-content-block crm-entity-widget-content-block-field-text" cid="PERSON">
                <div class="crm-entity-widget-content-block-title">
                    <span class="crm-entity-widget-content-block-title-text">Responsible Person</span>
                </div>
                <div class="crm-entity-widget-content-block-inner">
                    <div class="crm-entity-widget-content-block-field-container">
                        <input name="PERSON" class="crm-entity-widget-content-input" type="text" tabindex="4" />
                    </div>
                </div>
            </div>
        </td>
        <td>
            <div class="crm-entity-widget-content-block crm-entity-widget-content-block-field-custom-select" data-cid="SOURCE_ID">
                <div data-field-tag="SOURCE_ID" class="crm-entity-widget-before-action"></div>
                <div class="crm-entity-widget-content-block-title">
                    <span class="crm-entity-widget-content-block-title-text">Source</span>
                </div>
                <div class="crm-entity-widget-content-block-inner">
                    <span class="fields enumeration field-wrap">
                        <span class="fields enumeration enumeration-select field-item">
                            <?=CrmCardShowExtComponent::createSelect(
                                'SOURCE_ID', $arResult, 5
                            );?>
                        </span>
                    </span>
                </div>
            </div>
        </td>
    </tr>
</table>