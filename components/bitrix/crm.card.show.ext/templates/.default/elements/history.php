<?php if(!$arResult['HISTORY_HTML']) return; ?>
<br><br>
<div class="ui equal width grid">
    <div class="sixteen wide column">
        <h5 class="ui header">
            <i class="history icon"></i>
            <div class="content">
                Events History
            </div>
        </h5>
        <div style="padding: 5px 20px;"><?=htmlspecialcharsBack($arResult['HISTORY_HTML']);?></div>
    </div>
</div>